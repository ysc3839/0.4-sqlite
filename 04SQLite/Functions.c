#include "Functions.h"
#include "ConsoleUtils.h"
#include "squirrel.h"

extern HSQAPI sq;

_SQUIRRELDEF( ConnectSQL )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 )
	{
		// Get the path of the file
		const SQChar * path;
		sq->getstring( v, 2, &path );

		// Attempt to create an SQLite instance
		sqlite3 * pDb;
		int ret = sqlite3_open_v2( path, &pDb, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL );

		// See if we got it
		if( pDb != NULL && ret == SQLITE_OK )
		{
			sq->pushuserpointer( v, pDb );
			return 1;
		}
	}
	
	sq->pushnull( v );
	return 1;
}

_SQUIRRELDEF( DisconnectSQL )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 )
	{
		// Try to get the pointer to the database
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		// See if the pointer is valid
		if( pData != NULL )
		{
			sqlite3 * pDb = (sqlite3 *)(pData);
			if( pDb != NULL )
			{
				// Attempt to close the database
				int returnVal = sqlite3_close( pDb );
				if( returnVal == SQLITE_OK )
				{
					sq->pushbool( v, SQTrue );
					pDb = NULL;

					return 1;
				}
			}
		}
	}

	sq->pushbool( v, SQFalse );
	return 1;
}

_SQUIRRELDEF( QuerySQL )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 3 )
	{
		// Try to get the pointer to the database
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		// Try to get the query itself
		const SQChar * pszSQL = NULL;
		sq->getstring( v, 3, &pszSQL );

		// See if we have valid data
		if( pData != NULL && pszSQL != NULL )
		{
			// Cast to sqlite3*
			sqlite3 * pDb = (sqlite3 *)(pData);
			if( pDb != NULL )
			{
				// Make room for a statement
				sqlite3_stmt ** ppStmt = (sqlite3_stmt **)malloc( sizeof( sqlite3_stmt * ) );

				// Execute query
				int ret = sqlite3_prepare_v2(
					pDb,
					pszSQL,
					-1,
					ppStmt,
					NULL
				);

				if( ret == SQLITE_OK && ppStmt != NULL && (*ppStmt) != NULL )
				{
					// Attempt to step
					int ret2 = sqlite3_step( *ppStmt );
					if( ret2 == SQLITE_ROW )
					{
						// Return statement
						sq->pushuserpointer( v, ppStmt );
						return 1;
					}
					else
					{
						// Clean up, we failed hard
						sqlite3_finalize( *ppStmt );
						*ppStmt = NULL;
						 ppStmt = NULL;
					}
				}
			}
		}
	}

	sq->pushnull( v );
	return 1;
}

_SQUIRRELDEF( GetSQLNextRow )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 && sq->gettype( v, 2 ) == OT_USERPOINTER )
	{
		// Try to get pointer
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		if( pData != NULL )
		{
			// Cast to an sqlite3_stmt**
			sqlite3_stmt ** pStmt = (sqlite3_stmt **)(pData);
			if( pStmt != NULL && *pStmt != NULL )
			{
				// Go to the next row
				int ret = sqlite3_step( *pStmt );
				
				// No more rows
				if( ret == SQLITE_DONE )
				{
					// Clean up
					sqlite3_finalize( *pStmt );
					*pStmt = NULL;

					// Done
					sq->pushbool( v, SQFalse );
					return 1;
				}

				// We have another row
				else if( ret == SQLITE_ROW )
				{
					// Done
					sq->pushbool( v, SQTrue );
					return 1;
				}
			}
		}
	}

	sq->pushbool( v, SQFalse );
	return 1;
}

_SQUIRRELDEF( GetSQLColumnCount )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 && sq->gettype( v, 2 ) == OT_USERPOINTER )
	{
		// Try to get pointer
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		if( pData != NULL )
		{
			// Cast to an sqlite3_stmt**
			sqlite3_stmt ** pStmt = (sqlite3_stmt **)(pData);
			if( pStmt != NULL && *pStmt != NULL )
			{
				// Get the number of columns
				int colCount = sqlite3_column_count( *pStmt );

				// Push to Squirrel
				sq->pushinteger( v, colCount );

				// Done
				return 1;
			}
		}
	}

	sq->pushnull( v );
	return 1;
}

_SQUIRRELDEF( GetSQLColumnData )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 3 && sq->gettype( v, 2 ) == OT_USERPOINTER )
	{
		// Obtain pointer
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		if( pData != NULL )
		{
			// Cast to sqlite3_stmt**
			sqlite3_stmt ** pStmt = (sqlite3_stmt **)(pData);
			if( pStmt != NULL && *pStmt != NULL )
			{
				// Get the column index
				SQInteger colIdx;
				sq->getinteger( v, 3, &colIdx );

				// Get the type of column
				if( colIdx >= 0 )
				{
					switch( sqlite3_column_type( *pStmt, colIdx ) )
					{
						case SQLITE_INTEGER:
							sq->pushinteger( v, sqlite3_column_int( *pStmt, colIdx ) );
							return 1;

						case SQLITE_FLOAT:
							sq->pushfloat( v, sqlite3_column_double( *pStmt, colIdx ) );
							return 1;

						case SQLITE_TEXT:
						case SQLITE_BLOB:
							sq->pushstring( v, (char *)sqlite3_column_text( *pStmt, colIdx ), -1 );
							return 1;

						// Thanks to coincidental program flow, if we just break here,
						// the program will treat SQLITE_NULL the same as an error and
						// push a null value for us anyway.
						case SQLITE_NULL:
						default:
							break;
					}
				}
			}
		}
	}
	
	sq->pushnull( v );
	return 1;
}

_SQUIRRELDEF( FreeSQLQuery )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 && sq->gettype( v, 2 ) == OT_USERPOINTER )
	{
		// Try to get the pointer to the query
		SQUserPointer pData;
		sq->getuserpointer( v, 2, &pData );

		if( pData != NULL )
		{
			// Cast to sqlite3_stmt**
			sqlite3_stmt ** pStmt = (sqlite3_stmt **)(pData);
			if( pStmt != NULL )
			{
				// Clean up
				sqlite3_finalize( *pStmt );
				(*pStmt) = NULL;
				
				free( pStmt );
			}
		}
	}

	return 0;
}

_SQUIRRELDEF( escapeSQLString )
{
	SQInteger argc = sq->gettop( v );
	if( argc == 2 )
	{
		// Try to get the string
		const SQChar * pszSQL = NULL;
		sq->getstring( v, 2, &pszSQL );

		// See if we have valid data
		if( pszSQL != NULL )
		{
			char *ret;

			ret = sqlite3_mprintf("%q", pszSQL);
			if (ret) {
				sq->pushstring( v, (char *)ret, -1 );
				sqlite3_free(ret);
				return 1;
			}
		}
	}

	sq->pushnull( v );
	return 1;
}

SQInteger RegisterSquirrelFunc( HSQUIRRELVM v, SQFUNCTION f, const SQChar* fname, unsigned char ucParams, const SQChar* szParams )
{
	char szNewParams[ 32 ];

	sq->pushroottable( v );
	sq->pushstring( v, fname, -1 );

	/* create a new function */
	sq->newclosure( v, f, 0 );

	if ( ucParams > 0 ) 
	{
		/* This is to compensate for the root table */
		ucParams++;
		
		sprintf( szNewParams, "t%s", szParams );

		/* Add a param type check */
		sq->setparamscheck( v, ucParams, szNewParams );
	}

	sq->setnativeclosurename( v, -1, fname );
	sq->newslot( v, -3, SQFalse );

	/* pops the root table */
	sq->pop( v, 1 );

	return 0;
}

void RegisterFuncs( HSQUIRRELVM v )
{
	RegisterSquirrelFunc( v, ConnectSQL, "ConnectSQL", 1, "s" );
	RegisterSquirrelFunc( v, DisconnectSQL, "DisconnectSQL", 1, "p" );
	RegisterSquirrelFunc( v, QuerySQL, "QuerySQL", 2, "ps" );
	RegisterSquirrelFunc( v, GetSQLNextRow, "GetSQLNextRow", 1, "p|o" );
	RegisterSquirrelFunc( v, GetSQLColumnCount, "GetSQLColumnCount", 1, "p|o" );
	RegisterSquirrelFunc( v, GetSQLColumnData, "GetSQLColumnData", 2, "p|oi" );
	RegisterSquirrelFunc( v, FreeSQLQuery, "FreeSQLQuery", 1, "p|o" );
	RegisterSquirrelFunc( v, escapeSQLString, "escapeSQLString", 1, "s" );
}
