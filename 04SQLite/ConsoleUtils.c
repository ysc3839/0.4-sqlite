#include "ConsoleUtils.h"

void OutputDebug( const char * msg )
{
	#ifdef _DEBUG
		OutputMessage( msg );
	#endif
}

void OutputMessage( const char * msg )
{
	#ifdef WIN32
		HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );

		CONSOLE_SCREEN_BUFFER_INFO csbBefore;
		GetConsoleScreenBufferInfo( hstdout, &csbBefore );
		SetConsoleTextAttribute( hstdout, FOREGROUND_GREEN );
		printf("[MODULE]  ");
		
		SetConsoleTextAttribute( hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY );
		printf("%s\n", msg);

		SetConsoleTextAttribute( hstdout, csbBefore.wAttributes );
	#else
		printf( "%c[0;32m[MODULE]%c[0;37m %s\n", 27, 27, msg );
	#endif
}
