#ifdef WIN32
	#define WIN32_LEANANDMEAN
	#include <Windows.h>

	#define EXPORT __declspec(dllexport)
#else
	#define EXPORT
#endif

#include "VCMP.h"

#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

// Squirrel
#ifdef __cplusplus
extern "C"
{
#endif

	#include "squirrel/squirrel.h"

#ifdef __cplusplus
}
#endif

// A definition needed for Squirrel's print function
#ifdef SQUNICODE 
	#define scvprintf vwprintf 
#else 
	#define scvprintf vprintf 
#endif
