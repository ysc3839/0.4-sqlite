// Main include
#include "main.h"

// Console stuff
#include "ConsoleUtils.h"

// Module imports
#include "Functions.h"
#include "SqImports.h"

// Definitions
HSQAPI sq;
HSQUIRRELVM v;

// Global variables (meh)
PluginFuncs * gFuncs;

void OnSquirrelScriptLoad()
{
	// See if we have any imports from Squirrel
	size_t size;
	int     sqId            = gFuncs->FindPlugin( "SQHost2" );
	const void ** sqExports = gFuncs->GetPluginExports( sqId, &size );

	// We do!
	if( sqExports != NULL && size > 0 )
	{
		// Cast to a SquirrelImports structure
		SquirrelImports ** sqDerefFuncs = (SquirrelImports **)sqExports;
		
		// Now let's change that to a SquirrelImports pointer
		SquirrelImports * sqFuncs       = (SquirrelImports *)(*sqDerefFuncs);
		
		// Now we get the virtual machine
		if( sqFuncs )
		{
			// Get a pointer to the VM and API
			sq = *(sqFuncs->GetSquirrelAPI());
			v = *(sqFuncs->GetSquirrelVM());

			// Register functions
			RegisterFuncs( v );
		}
	}
	else
		OutputMessage( "Failed to attach to SQHost2." );
}

uint8_t OnInternalCommand(unsigned int uCmdType, const char* pszText)
{
	switch( uCmdType )
	{
		case 0x7D6E22D8:
			OnSquirrelScriptLoad();
			break;

		default:
			break;
	}

	return 1;
}

uint8_t OnInitServer()
{
	printf( "\n" );
	OutputMessage( "Loaded SQLite3 for VC:MP by Stormeus." );

	return 1;
}

#ifdef __cplusplus
extern "C"
#endif
EXPORT unsigned int VcmpPluginInit( PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info )
{
	// Set our plugin information
	info->pluginVersion = 0x1100; // 1.1.00
	info->apiMajorVersion = PLUGIN_API_MAJOR;
	info->apiMinorVersion = PLUGIN_API_MINOR;
	strcpy( info->name, "SQLite3 for VC:MP" );

	// Store functions for later use
	gFuncs = functions;

	// Store callback
	callbacks->OnServerInitialise = OnInitServer;
	callbacks->OnPluginCommand = OnInternalCommand;

	// Done!
	return 1;
}
